#define BOOST_TEST_MODULE Node Test
#include <boost/test/unit_test.hpp>
#include <interthd/interthd.h>

BOOST_AUTO_TEST_CASE(test)
{
    interthd::Node node;
    int sum = 1 + 1;
    BOOST_TEST(sum == 2);
}